package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.cms.CmsConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CmsConfigControllerApi   
 * @package    com.xuecheng.api.cms  
 * @date   2019/12/16 15:49  
 * @explain
 */
@Api(value = "cms配置管理接口",description = "cms页面的管理接口,提供数据模型的管理、查询接口")
public interface CmsConfigControllerApi {

    @ApiOperation("根据id查询cms配置信息")
    public CmsConfig getModel(String id);

}
