package com.xuecheng.framework.exception;

import com.xuecheng.framework.model.response.ResultCode;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     ExceptionCast   
 * @package    com.xuecheng.framework.exception  
 * @date   2019/12/16 11:24  
 * @explain
 */
public class ExceptionCast {

    // 使用此静态方法抛出自定义异常
    public static void cast(ResultCode resultCode){
        throw new CustomException(resultCode);
    }

}
