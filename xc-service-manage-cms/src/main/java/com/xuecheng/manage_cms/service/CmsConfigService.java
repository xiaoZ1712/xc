package com.xuecheng.manage_cms.service;

import com.xuecheng.framework.domain.cms.CmsConfig;
import com.xuecheng.manage_cms.dao.CmsConfigRepoistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CmsConfigService   
 * @package    com.xuecheng.manage_cms.service  
 * @date   2019/12/16 15:53  
 * @explain
 */
@Service
public class CmsConfigService {

    @Autowired
    private CmsConfigRepoistory cmsConfigRepoistory;

    // 根据id查询配置管理信息
    public CmsConfig getConfigById(String id){
        Optional<CmsConfig> optional = cmsConfigRepoistory.findById(id);
        if (optional.isPresent()){
            CmsConfig cmsConfig = optional.get();
            return cmsConfig;
        }
        return null;
    }

}
