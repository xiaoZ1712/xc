package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.CmsTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CmsPageRepository   
 * @package    com.xuecheng.manage_cms.dao  
 * @date   2019/10/31 15:47  
 * @explain
 */
//@ResponseBody
public interface CmsTemplateRepository extends MongoRepository<CmsTemplate,String> {

}
