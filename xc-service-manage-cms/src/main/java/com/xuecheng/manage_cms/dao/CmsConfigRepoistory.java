package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsConfig;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CmsConfigRepoistory   
 * @package    com.xuecheng.manage_cms.dao  
 * @date   2019/12/16 15:51  
 * @explain
 */
public interface CmsConfigRepoistory extends MongoRepository<CmsConfig,String> {
}
