package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CmsPageRepository   
 * @package    com.xuecheng.manage_cms.dao  
 * @date   2019/10/31 15:47  
 * @explain
 */
//@ResponseBody
public interface CmsPageRepository extends MongoRepository<CmsPage,String> {

    // 根据页面名称查询
    CmsPage findByPageName(String pageName);

    // 根据页面名称和类型查询
    CmsPage findByPageNameAndPageType(String pageName,String pageType);

    //根据站点和页面类型查询记
    int countBySiteIdAndPageType(String siteId,String pageType);

    //根据站点和页面类型分页查询
    Page<CmsPage> findBySiteIdAndPageType(String siteId, String pageType, Pageable pageable);

    // 根据页面名\站点id\页面访问路径查询(保证页面唯一)
    CmsPage findByPageNameAndSiteIdAndPageWebPath(String pageName,String siteId,String pageWebPath);

}
