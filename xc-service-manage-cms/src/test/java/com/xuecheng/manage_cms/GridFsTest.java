package com.xuecheng.manage_cms;

import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     GridFsTest   
 * @package    com.xuecheng.manage_cms  
 * @date   2019/12/16 17:55  
 * @explain
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class GridFsTest {
    @Autowired
    GridFsTemplate gridFsTemplate;

    @Autowired
    GridFSBucket gridFSBucket;

    @Test
    public void queryFile() throws IOException {


        String fileId = "5a795bbcdd573c04508f3a59";
        GridFSFile fsFile = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(fileId)));
        // 打开下载流对象
        GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(fsFile.getObjectId());
        // 创建GridFsResource用于获取流对象
        GridFsResource fsResource = new GridFsResource(fsFile, gridFSDownloadStream);
        // 获取流中的数据
        String s = IOUtils.toString(fsResource.getInputStream(), "utf-8");
        System.out.println(s);

    }


    @Test
    public void testGridFs() throws FileNotFoundException { //要存储的文件
        String classpath = this.getClass().getResource("/").getPath();
        File file = new File("E:\\xcCode\\XcEduCode01\\xc-framework-parent\\xc-service-manage-cms\\src\\test\\resources\\index_banner.ftl");
        //定义输入流
        FileInputStream inputStram = new FileInputStream(file);
        //向GridFS存储文件
        ObjectId objectId = gridFsTemplate.store(inputStram, "轮播图测试文件01", "");
        //得到文件ID
        String fileId = objectId.toString();
        System.out.println(fileId);
    }


    @Test
    public void testDelFile() {
        //  根据id删除fs。files和fs。chunks中的记录
        gridFsTemplate.delete(Query.query(Criteria.where("_id").is("5df75565c2720c2f38b97a3b")));
    }
}
