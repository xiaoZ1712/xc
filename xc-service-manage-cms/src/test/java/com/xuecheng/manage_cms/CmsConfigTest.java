package com.xuecheng.manage_cms;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CmsConfigTest   
 * @package    com.xuecheng.manage_cms  
 * @date   2019/12/16 16:06  
 * @explain
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CmsConfigTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    GridFsTemplate gridFsTemplate;

    @Test
    public void testRestTemplate() {
        ResponseEntity<Map> forEntity = restTemplate.getForEntity("http://localhost:31001/cms/config/getmodel/5a791725dd573c3574ee333f", Map.class);
        System.out.println(forEntity);
    }


    @Test
    public void testGridFs() throws IOException {
        //要存储的文件
        File file = new File("E:/static-html");
        File ftlFiles = file.getCanonicalFile();
        // 定义输入流
        FileInputStream inputStram = new FileInputStream(file);
        // 向GridFS存储文件
        ObjectId objectId = gridFsTemplate.store(inputStram, "轮播图测试文件01", "");
        //得到文件ID
        String fileId = objectId.toString();
        System.out.println(fileId);
    }

}
