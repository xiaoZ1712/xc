package com.xuecheng.manage_cms;

import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.CmsPageParam;
import com.xuecheng.manage_cms.dao.CmsPageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CmsPageRepoistoryTest   
 * @package    cms.xuechent.mange_cms  
 * @date   2019/10/31 15:49  
 * @explain
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CmsPageRepoistoryTest {

    @Autowired
    private CmsPageRepository cmsPageRepository;


    // 分页测试
    @Test
    public void testFindPage(){

        int page = 0; // 从page为0开始
        int size = 10; // 每页10条
        Pageable pageable = PageRequest.of(page, size);
        Page<CmsPage> all = cmsPageRepository.findAll(pageable);
        System.out.println("============Result:"+all);
    }

    /**
     * 测试添加
     */
    @Test
    public void testAdd(){
        CmsPage cmsPage = new CmsPage();
        cmsPage.setSiteId("s01");
        cmsPage.setTemplateId("t01");
        cmsPage.setPageName("测试页面");
        cmsPage.setPageCreateTime(new Date());
        List<CmsPageParam> cmsPageParams = new ArrayList<>();
        CmsPageParam cmsPageParam = new CmsPageParam();
        cmsPageParam.setPageParamName("param1");
        cmsPageParam.setPageParamValue("value1");
        cmsPageParams.add(cmsPageParam);
        cmsPage.setPageParams(cmsPageParams);
        cmsPageRepository.save(cmsPage);
        System.out.println(cmsPage);
    }

    /**
     * 测试删除
     */
    @Test
    public void testDel(){
        cmsPageRepository.deleteById("5dba9c90400edf2cf03940e5");
    }


    /**
     * 测试修改
     */
    @Test
    public void testupdate(){
        Optional<CmsPage> byId = cmsPageRepository.findById("5af942190e661827d8e2f5e3");
        if (byId.isPresent()){
            CmsPage cmsPage = byId.get();
            cmsPage.setPageName("测试页面");
            cmsPageRepository.save(cmsPage);
        }
    }

    /**
     * 测试自定义查询条件
     */
    @Test
    public void testCustomerCondition(){
        //条件匹配器
        ExampleMatcher matching = ExampleMatcher.matching();
        matching.withMatcher("pageAliase",ExampleMatcher.GenericPropertyMatchers.contains());
        // 页面别名模糊查询
        //ExampleMatcher.GenericPropertyMatchers.contains() 包含
        //ExampleMatcher.GenericPropertyMatchers.startsWith() 开头匹配
        // 条件值
        CmsPage cmsPage = new CmsPage();
        cmsPage.setSiteId("5a751fab6abb5044e0d19ea1");
        //模板ID
        cmsPage.setTemplateId("5a925be7b00ffc4b3c1578b5");
        //创建条件实例
        Example<CmsPage> example = Example.of(cmsPage, matching);
        Pageable pageable = PageRequest.of(0,10);
        Page<CmsPage> all = cmsPageRepository.findAll(example, pageable);
        System.out.println(all.iterator().next());
    }

    @Test
    public void test1() {
        Date date = new Date();
        System.out.println(date);
    }

}
